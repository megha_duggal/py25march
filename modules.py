# import random
# num=random.randint(100,200)
# print(num)

# from random import randint 
# num=randint(100,200)
# print(num)

# import random as r       #to rename any function
# num=r.randint(100,200)
# print(num)

from random import randint as r
print(r(100,200))

print(__name__)            #this shows main bcz this can not be imported
