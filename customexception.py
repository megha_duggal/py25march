class MyError(Exception):
    def __init__(self,v):
        self.v=v
    def __str__(self):
        return self.v

try:
    print('Code inside try block')
    raise MyError(404)

except MyError as a:
    print('Code inside except block') 
    print('Error:{}User Defined Exception'.format(a.v))   
